import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import ShowAccount from './ShowAccount';
import { Link } from 'react-router-dom';

export default function Account() {
    let { path, url } = useRouteMatch();

    return (

        <div style={{ marginLeft: "200px" }}>
            <h1>Account</h1>
            <ul>
                <li>
                    <Link to={`${url}/netflix`}>Netflix</Link>
                </li>
                <li>
                    <Link to={`${url}/zillo-group`}>Zillo Group</Link>
                </li>
                <li>
                    <Link to={`${url}/yahho`}>Yahho</Link>
                </li>
                <li>
                    <Link to={`${url}/modus-create`}>Modus Create</Link>
                </li>
            </ul>
            <Switch>
                <Route path={`${path}/:id`} component={ShowAccount} />
            </Switch>
        </div>
    )
}

