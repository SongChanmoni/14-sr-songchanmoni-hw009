import React from 'react'
import { Link, useLocation, useRouteMatch } from 'react-router-dom'
import queryString from 'query-string'
import { Button, ButtonGroup } from 'react-bootstrap'

export default function Movie() {
    let { url } = useRouteMatch();
    const location = useLocation()
    const query = queryString.parse(location.search)
    console.log(query);
    return (
        <div>
            <h1>Movie Category</h1>
            <ButtonGroup aria-label="Basic example">
                <Button as={Link} to={`${url}?type=Adventure`} variant="secondary">Adventure</Button>
                <Button as={Link} to={`${url}?type=Crime`} variant="secondary">Crime</Button>
                <Button as={Link} to={`${url}?type=Action`} variant="secondary">Action</Button>
                <Button as={Link} to={`${url}?type=Romance`} variant="secondary">Romance</Button>
                <Button as={Link} to={`${url}?type=Comedy`} variant="secondary">Comedy</Button>
            </ButtonGroup>

            <h2  style={{marginTop:"10px"}}>Please Choose Catogory : <span style={{ color: "blue" }}>{query.type}</span></h2>

        </div>
    )
}
