import React from 'react'
import { useRouteMatch, Switch, Route,BrowserRouter as Router } from 'react-router-dom'
import Animation from './Animation';
import Movie from './Movie';
import { Button,ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Video() {
    let { path, url } = useRouteMatch();
    return (
        <div style={{ marginLeft: '200px' }}>
            <h1> Video</h1>
            <ButtonGroup aria-label="Basic example">
                <Button as={Link} to={`${url}/movie`} variant="secondary">Movie</Button>
                <Button as={Link} to={`${url}/animation`} variant="secondary">Animation</Button>
            </ButtonGroup>
         
                <Switch>
                    <Route path={`${path}/movie`} component={Movie} />
                    <Route path={`${path}/animation`} component={Animation} />
                   
                </Switch>
        </div>
    )
}

