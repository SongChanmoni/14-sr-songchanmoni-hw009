import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './components/Menu/Menu'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './components/Home/Home';
import Video from './components/Video/Video';
import Auth from './components/Auth';
import Welcome from './components/Welcome';
import Detail from './components/Home/Detail';
import Account from './components/Account/Account';

function App() {
  return (
    <div className="App">
      <Router>
        <Menu />
        <Switch>

          <Route exact path='/' component={Home} />
          <Route path='/detail/:id' component={Detail} />
          <Route path='/Video' component={Video} />
          <Route path='/Account' component={Account} />
          <Route path='/Welcome' component={Welcome} />
          <Route path='/Auth' component={Auth} />

        </Switch>


      </Router>


    </div>
  );
}

export default App;
