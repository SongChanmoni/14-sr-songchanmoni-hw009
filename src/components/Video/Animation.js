import React from 'react'
import {useRouteMatch } from 'react-router'
import { useLocation } from 'react-router'
import { Link } from 'react-router-dom'
import queryString from 'query-string'
import { Button, ButtonGroup } from 'react-bootstrap'

export default function Animation() {

    let { path, url } = useRouteMatch();
    const location = useLocation();
    const query = queryString.parse(location.search)
    

    return (
        <div>
             <h1>Animation Category</h1>
            <ButtonGroup aria-label="Basic example">
                <Link to={`${url}?type=Action`}><Button variant="secondary">Action</Button></Link>
                <Link to={`${url}?type=Romance`}><Button variant="secondary">Romance</Button></Link>
                <Link to={`${url}?type=Comedy`}><Button variant="secondary">Comedy</Button></Link>
            </ButtonGroup>

            <h2 style={{marginTop:"10px"}}>Please Choose Catogory : <span style={{ color: "blue" }}>{query.type}</span></h2>
            
        </div>
    )
}
