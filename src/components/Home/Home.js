import React, { Component } from 'react'
import { Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default class Home extends Component {
    constructor() {
        super()
        

        this.state = {
            car: [
                {
                    id:"1",
                    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMGd0mPp71GZ3c-VAOyEXmzKNc_mRcqV6wfPD5rrWmoAccV-SlXaAR6BvifnoohwCjh74&usqp=CAU",
                    title: "Lamborghini",
                    desc: "Lamborghini new 2021"
                },
                {
                    id:"2",
                    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThZtP9oXCD7CTBIFSG54EZgQVn9CQZYc9jfLkLg4_2OmeZeWjU_OqsYr7yVd6OKwzJ08E&usqp=CAU",
                    title: "BMW",
                    desc: "BMW new 2021",
                },
                {
                    id:"3",
                    image: "https://www.driving.co.uk/s3/st-driving-prod/uploads/2018/08/Rolls-Royce-Phantom-Privacy-Suite-01.jpg",
                    title: "Rolls royce",
                    desc: "Rolls royce new 2021",
                },
                {
                    id:"4",
                    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSySopu0AGpdhHBR4R5Cy2qUtpzMMXC8AXwdsVADxignqhP2SXUyLRyBvokvFEkMEeFuds&usqp=CAU",
                    title: "Rolls royce",
                    desc: "Rolls royce new 2021",
                }
            ]
        }
        
    }
    


    render() {
        return (
            <div>
                <div class="container-fluid w-100  mt-5 ">
                    <div className="row"  >
                        {this.state.car.map((item, index) => {
                            return (
                                <div className="col-lg-3  "   >
                                    <Card className="" style={{ width: '18rem' }}>
                                        <Card.Img variant="top" src={item.image} />
                                        <Card.Body>
                                            <Card.Title>{item.title}</Card.Title>
                                            <Card.Text>
                                                {item.desc}
                                            </Card.Text>
                                            <Button as={Link} to={`/detail/${item.id}`} variant="primary">Read</Button>
                                        </Card.Body>
                                    </Card>
                                </div>
                            )
                        })}
                    </div>
                </div>



            </div>
        )
    }
}
